﻿// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (c) Lieberman Technologies, LLC.  All rights reserved.
// Copyright file: ICarService.cs  Copyright project: AjaxWithRazorPagesDemo
// Created: 09, 02, 2022
// Modified: 09, 02, 2022
// ---------------------------------------------------------------------------------------------------------------------------------

using AjaxWithRazorPagesDemo.Data.Models;

namespace AjaxWithRazorPagesDemo.Data;

public interface ICarService
{
    List<Car>? GetAll();
}
