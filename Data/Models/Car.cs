﻿// ---------------------------------------------------------------------------------------------------------------------------------
// Copyright (c) Lieberman Technologies, LLC.  All rights reserved.
// Copyright file: Car.cs  Copyright project: AjaxWithRazorPagesDemo
// Created: 09, 02, 2022
// Modified: 09, 02, 2022
// ---------------------------------------------------------------------------------------------------------------------------------

namespace AjaxWithRazorPagesDemo.Data.Models;

public class Car
{
    public int Id { get; set; }

    public string? Make { get; set; }

    public string? Model { get; set; }

    public int Year { get; set; }

    public int Doors { get; set; }

    public string? Colour { get; set; }

    public decimal Price { get; set; }
}
