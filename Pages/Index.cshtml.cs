﻿using AjaxWithRazorPagesDemo.Data;
using AjaxWithRazorPagesDemo.Data.Models;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace AjaxWithRazorPagesDemo.Pages;

public class IndexModel : PageModel
{
    private readonly ICarService _carService;

    public List<Car>? Cars { get; set; }

    public IndexModel(ICarService carService) { _carService = carService; }

    public void OnGet() { }

    public PartialViewResult OnGetCarPartial()
    {
        Cars = _carService.GetAll();

        // Partial(viewName, model)
        return Partial("_CarPartial", Cars);
    }
}
